# Demo-ui Angular

This project is the front end part of the Demo Application. It takes care of user login and data managemnet

## Prerequirents

- [Node](https://nodejs.org/en/)
- [Angular CLI](https://cli.angular.io/)

## Set up this Angular project in Eclipse

Lets launch our Eclipse IDE. First we will have to download the Angular Js IDE extentions, to do that please go to Help > Eclipse Market > Place find Angular Js IDE and click install.
To import the project go File > Import > Angular > existing angular project. Please browes to the angular project folder and select it and that should be it to view our source code

##  How to set up and run  the project
Please download this project. To initiliaze it and to get all it's dependencies please navigate into it's root folder with your prefered command tool.
run the following command ```npm install ``` This will download all the dependencies.
To run the application please run the following command ```npm start ``` this will serve the application on port 4200
