import { TestBed } from '@angular/core/testing';

import { PetStoreService } from './petstore.service';

describe('PetstoreService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PetStoreService = TestBed.get(PetStoreService);
    expect(service).toBeTruthy();
  });
});
