import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Category } from '../interfaces/category';
import { AuthService } from '../services/auth.service';


@Injectable()
export class PetStoreService {

  constructor(private http: HttpClient, private authService: AuthService) { }

  /**
  * requets products from server
  */
  public getProducts() {
    return this.http.get('/server/api/petstore', this.getHeaders());
  }

 /**
  * requets categories from server
  */
  public getCategories(): Observable<Category[]> {
        return this.http.get<Category[]>('/server/api/categories/', this.getHeaders());
  }

  public getProduct(id: number) {
    return this.http.get('/server/api/petstore/' + id, this.getHeaders());
  }

 /**
  * post new product to server
  */
  public createNewProduct(aProduct) {
    /*const body = JSON.stringify(aProduct);*/
    return this.http.post('/server/api/petstore/', aProduct, this.getHeaders());
  }

 /**
  * deletes products from server
  */
  public deleteProduct(id: number) {
    return this.http.delete('/server/api/petstore/' + id, this.getHeaders());
  }

  /**
  * updates product on server
  */
  public updateProduct(id: number, aProduct) {
    const body = JSON.stringify(aProduct);
    return this.http.put('/server/api/petstore/' + id, body, this.getHeaders());
  }

  /**
  * generates HTTP headers
  */
  private getHeaders() {

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Authorization': 'Basic ' + btoa(this.authService.getUser() + ':' + this.authService.getPwd()),
        'Access-Control-Allow-Origin' : '*'
      })
    };

    return httpOptions;
  }


}
