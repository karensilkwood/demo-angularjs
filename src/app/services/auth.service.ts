import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { AuthRequest } from '../interfaces/AuthRequest';
import { AuthResponse } from '../interfaces/AuthResponse';




const httpOptions = {
  headers : new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable()
export class AuthService {

  constructor(private http: HttpClient, private router: Router) { }


  /**
  * login user against server response to see if its accaptable to use if so allaw access to routes
  */
  public login(aAuthObj: AuthRequest, pwd: string) {

    console.log('In AuthService:login');

    this.getAuthFeedback(JSON.stringify(aAuthObj)).subscribe(
          data => {

          console.log('In AuthService:login->getAuthFeedback data recived', data);
          console.log('In AuthService:login->userWithEmailExists', data);

            if ( data && data.UserWithEmailExists) {

                console.log('In AuthService:user exist');

                localStorage.setItem('user_email', aAuthObj.graphisoftUserDto.emailAddress);
                localStorage.setItem('user_pwd', pwd);

                 console.log('In AuthService:roying admin begins');
                this.router.navigate(['/admin']);

            }
          }
        );

   }

 /**
  * log user out
  */
  public logout(): void {
    // Remove user data from localStorage
    localStorage.removeItem('user_email');
    localStorage.removeItem('user_pwd');

    // Go back to the home route
    this.router.navigate(['/']);
  }

  /**
  * isAuthenticated trackers
  */
  public isAuthenticated(): boolean {
    console.log('In AuthService:isAuthenticated');

    if (localStorage.getItem('user_email')) {
            // logged in so return true
          console.log('In AuthService:isAuthenticated TRUE');

            return true;
        }
        // not logged in so redirect to login page with the return url
        console.log('In AuthService:isAuthenticated FALSE');
       this.router.navigate(['/']);
        return false;
    }

    /**
     * get user from local storage
     */
    public getUser(): string {
      return localStorage.getItem('user_email');
    }

    /**
     * get user pwd from local storage
     */
    public getPwd(): string {
      return localStorage.getItem('user_pwd');
    }

    /**
     * autherntication entry point
     */
    public getAuthFeedback(body): Observable<AuthResponse> {
      // tslint:disable-next-line:max-line-length
      return this.http.post<AuthResponse>('https://graphisoftid-api-test.graphisoft.com/api/Account/PostIsUserWithEmailExists', body, httpOptions);
  }

}
