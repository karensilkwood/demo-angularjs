import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthService } from '../../services/auth.service';
import { AuthRequest } from '../../interfaces/AuthRequest';
import { Auth } from '../../interfaces/Auth';



@Component({
  selector: 'app-callback',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.less']
})
export class LoginComponent implements OnInit {

  signinForm: FormGroup;

  constructor(private authService: AuthService) { }

  ngOnInit() {

    this.signinForm = new FormGroup({
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', Validators.required)
    });

  }

  /**
  * try log in user with credentials
  */
  public submitLogin() {
    if (this.signinForm.valid) {
        console.log(this.signinForm.value);

        const request = {} as AuthRequest;
        const email = {} as Auth;
        email.emailAddress = this.signinForm.get('email').value;
        request.graphisoftUserDto = email;

        this.authService.login(request, this.signinForm.get('password').value);

    }
  }

}
