import { Component, OnInit } from '@angular/core';
import { PetStoreService } from '../../services/petstore.service';
import { Category } from '../../interfaces/category';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Observable } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { AuthService } from '../../services/auth.service';
import { ValidatorSupportedEmail } from '../../utils/validators/validator.supportedemail';


@Component({
  selector: 'app-home',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.less']
})

export class ItemComponent implements OnInit {

  productform: FormGroup;
  validMessage = '';
  isModify = false;
  categories: Category[];

  // tslint:disable-next-line:max-line-length
  constructor(private petStoreService: PetStoreService, private router: ActivatedRoute, private authService: AuthService , private fb: FormBuilder) { }

  ngOnInit() {

    this.getCategories();

    /*init form */
    this.productform = this.fb.group({
      category: ['', Validators.required],
      name: ['', [Validators.required, Validators.minLength(6)]],
      description: [''],
      price: ['', Validators.required],
      author: ['', [Validators.required, Validators.minLength(5)]],
      // tslint:disable-next-line:max-line-length
      email: [ this.authService.getUser(), [Validators.required, Validators.email], ValidatorSupportedEmail.validateEmail(this.authService)],
      uploadDate: ['', Validators.required],
    });

    const currId = this.router.snapshot.params.id;
    if (currId) {
      this.isModify = true;
      this.petStoreService.getProduct(currId).subscribe(
        data => {
          this.productform.patchValue(data);
        }
      );
    }
  }


  /**
  * Submits form data to server
  */
  public submitRegistration() {

    if (this.productform.valid) {

      if (this.isModify) {
        this.petStoreService.updateProduct(this.router.snapshot.params.id,  this.productform.value).subscribe(
        data => {
          this.validMessage = 'Your product update has been submited thank you';
          this.productform.reset();
          return true;
        },
        error => {
          return Observable.throw(error);
        }
      );
      } else {

      this.petStoreService.createNewProduct( this.productform.value).subscribe(
        data => {
          this.validMessage = 'Your new product has been created and submited thank you';
          this.productform.reset();
          return true;
        },
        error => {
          return Observable.throw(error);
        }
        );
      }
    } else {
      this.validMessage = 'There are validation  errors on this page! Please fill out manadantory fields before submitting';
    }
  }

  /**
  * Gets available categories from Server
  */
  private getCategories() {
    this.petStoreService.getCategories().subscribe(
      data => {
        console.log('Category data:', data);
        this.categories = data;
          return true;
        },
        error => {
          return Observable.throw(error);
        }
    );
  }
  
  
  /**
  * navigate back to list
  */
  public onBackToList() {
      this.router.navigate(['/admin']);
  }
}
