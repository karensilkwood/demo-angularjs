import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PetStoreService } from '../../services/petstore.service';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.less']
})
export class AdminComponent implements OnInit {

  public products;

  constructor(private petStoreService: PetStoreService, private router: Router) { }

  ngOnInit() {
    this.getProducts();
  }

  /**
  * Get products and save it local variable
  */
  private getProducts() {
    this.petStoreService.getProducts().subscribe(
      data => {this.products = data; },
      err => console.error(err),
      () => console.log('products loaded')
    );
  }

 /**
  * Open product edit page
  */
  public openEdit(id) {
      this.router.navigate(['/admin/view', id]);
  }

  /**
  * delete product and reload data on success
  */
  public deleteItem(id) {
     this.petStoreService.deleteProduct(id).subscribe(
      data => {this.products = data; },
      err => console.error(err),
      () => console.log('product deleted')
    );
  }

 /**
  * Open add new item page
  */
  public addNewItem() {
    this.router.navigate(['/admin/view']);
  }
}
