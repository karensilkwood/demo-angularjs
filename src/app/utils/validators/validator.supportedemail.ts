import { AbstractControl } from '@angular/forms';
import { AuthService } from '../../services/auth.service';
import { AuthRequest } from '../../interfaces/AuthRequest';
import { Auth} from '../../interfaces/Auth';


export class ValidatorSupportedEmail {

 /*
  * Email Validator, validate email against server response to whatever the email is useable
  */
    static validateEmail(authService: AuthService): {[key: string]: any} {
            return (control: AbstractControl) => {
                return new Promise( resolve => {

                const request = {} as AuthRequest;
                const email = {} as Auth;
                email.emailAddress = control.value;
                request.graphisoftUserDto = email;
// tslint:disable-next-line:no-trailing-whitespace

                authService.getAuthFeedback(request).subscribe(
                    data => {
                        if ( data && data.UserWithEmailExists) {
                        resolve(null);
                        } else {
                            resolve ({ error: 'email is not supported'});
                        }
                     }
                    );
                });
            };
        }
    }
