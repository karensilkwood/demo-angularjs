import { Auth } from './Auth';

export interface AuthRequest {
    graphisoftUserDto: Auth;
}
