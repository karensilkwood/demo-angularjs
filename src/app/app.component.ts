import { Component } from '@angular/core';

import { AuthService } from './services/auth.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})
export class AppComponent {
  title = 'demo-ui';

  constructor(private authService: AuthService) {}

/**
*log out user
*/
  public logout() {
      this.authService.logout();
  }

/**
*check if user is logged in our not
*/
  public isUserLoginIn(): boolean {
    return this.authService.getUser() ? true : false;
  }

}
